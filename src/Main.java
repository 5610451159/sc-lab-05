public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Library library = new Library() ;
		
		Student student = new Student("Priyapat Iamsangjun", "5610451159", "D14") ;
		Teacher teacher = new Teacher("Chavalit Srisathapornphat", "D1411") ;
		
		Book book1 = new Book("Big Java") ;
		Book book2 = new Book("Python") ;
		Book book3 = new Book("Data Base") ;
		Book book4 = new Book("Data Structure") ;
		Book book5 = new Book("C++") ;
		library.addBook(book1) ;
		library.addBook(book2) ;
		library.addBook(book3) ;
		library.addBook(book4) ;
		library.addBook(book5) ;
		
		ReferenceBook reference1 = new ReferenceBook("Newspaper") ;
		ReferenceBook reference2 = new ReferenceBook("Magazine") ;
		ReferenceBook reference3 = new ReferenceBook("Fiction Book") ;
		ReferenceBook reference4 = new ReferenceBook("Comic Book") ;
		ReferenceBook reference5 = new ReferenceBook("Novel") ;
		library.addReferenceBook(reference1) ;
		library.addReferenceBook(reference2) ;
		library.addReferenceBook(reference3) ;
		library.addReferenceBook(reference4) ;
		library.addReferenceBook(reference5) ;
		
		System.out.println("Total books : " + library.getBookAmount() + "\n") ;
		System.out.println("Library User : " + student.getName());
		System.out.println("Borrow Book : " + library.borrowBook("Big Java")) ; //˹ѧ��Ͷ١��� 1 ���� ����� 6 ����
		System.out.println("Total books : " + library.getBookAmount() + "\n") ;
		System.out.println("Borrow Book : " + library.borrowBook("Newspaper")) ; //˹ѧ��Ͷ١��� 1 ���� ����˹ѧ��ͷ���������ö����� ����� 6 ����
		System.out.println("Total books : " + library.getBookAmount() + "\n") ;
		System.out.println("Return Book : " + library.returnBook("Big Java")) ; //˹ѧ��Ͷ١�׹ 1 ���� ����� 7 ����
		System.out.println("Total books : " + library.getBookAmount() + "\n") ;
	}

}
